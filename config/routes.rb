Rails.application.routes.draw do
  devise_for :users
  
  authenticated :user do
    root to: "dashboard#index", as: :authenticated_root
    resources :cart_itens, only: [:create, :update, :destroy]
  end
 
  unauthenticated :user do
    root to: "home#index"
  end
  resources :brands, only: [:index, :show]
  resources :search, only: [:index], as: :searches
  
  get "/cart", to: "cart#index", as: :cart
  get "/address", to: "address#index", as: :address
end
